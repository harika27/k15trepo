package com.k15t.pat.registration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.k15t.entity.Registration;
import com.k15t.pat.ApplicationBootstrap;
import com.k15t.repository.RegistrationRepo;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationBootstrap.class})
public class RegistrationResourceTest {
    
  
    private MockMvc mockMvc;
    
    
    @Mock
    RegistrationResource repo;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    
    @Before
    public void setup() {
            mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).defaultRequest(MockMvcRequestBuilders.post("/save")).build();
    }

   @Test
    public void test_Save() throws Exception {
        Registration regData = new Registration();
        regData.setName("Kfifteent");
        regData.setPassword("k15tpwd");
        regData.setCountry("India");
        regData.setState("Telangana");
        regData.setCity("Hyderabad");
        regData.setZip_code("500083");
        regData.setPhoneNumber("9392885968");
              
       //Mockito.when(repo.save(Mockito.any(Registration.class))).thenReturn(regData);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/save"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    }
   
  /* @Test
   public void test_Save_Fail() throws Exception {
       Registration regData = new Registration();
       regData.setName("K15t");
       regData.setPassword("k15tpwd");
       regData.setCountry("India");
       regData.setState("Telangana");
       regData.setCity("Hyderabad");
       regData.setZip_code("500083");
       regData.setPhoneNumber("9392885968");
    
      
       this.mockMvc.perform(MockMvcRequestBuilders.post("/save"))
       .andExpect(MockMvcResultMatchers.status().isInternalServerError());
 

   }*/
    

}
