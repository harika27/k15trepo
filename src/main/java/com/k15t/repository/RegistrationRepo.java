package com.k15t.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.k15t.entity.Registration;

@Repository
public interface RegistrationRepo extends CrudRepository<Registration, Integer>{
    
      
}
