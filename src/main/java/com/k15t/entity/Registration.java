package com.k15t.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "Meet_Up_Registrations")
public class Registration {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    
    @Column(name = "Name")
    @Pattern(regexp="^[a-zA-Z]+$", message ="Only Characters are allowed in Name")
    private String name;
    
    @Column(name = "Password")
    private String password;
    
    @Column(name = "Country")
    private String country;
    
    @Column(name = "City")
    private String city;
    
    @Column(name = "State")
    private String state;
    
    @Column(name = "Zip_Code")
    private String zip_code;
    
    @Column(name = "Email")
    @Email(message = "Enter a valid Email address.") 
    private String email;
    
    @Column(name = "Phone_number")
    @Pattern(regexp="^$|^[0-9]+$", message ="Only numbers are allowed in Phone Number")
    private String phoneNumber;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    
    public String getId() {
        return id;
    }

    
    public String getCountry() {
        return country;
    }

    
    public void setCountry(String country) {
        this.country = country;
    }

    
    public String getCity() {
        return city;
    }

    
    public void setCity(String city) {
        this.city = city;
    }

    
    public String getState() {
        return state;
    }

    
    public void setState(String state) {
        this.state = state;
    }

    
    public String getZip_code() {
        return zip_code;
    }


    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }
    
    @Override
    public String toString() {
        return "Registration [id=" + id + ", name=" + name + ", password=" + password + ", country=" + country + ", city=" + city
                + ", state=" + state + ", zip_code=" + zip_code + ", email=" + email + ", phoneNumber=" + phoneNumber + "]";
    }

    
 
}
