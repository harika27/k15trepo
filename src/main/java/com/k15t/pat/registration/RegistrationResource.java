package com.k15t.pat.registration;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.k15t.entity.Registration;
import com.k15t.repository.RegistrationRepo;

@RestController
public class RegistrationResource {

    @Autowired
    RegistrationRepo repo;

    public RegistrationRepo getRepo() {
        return repo;
    }

    public void setRepo(RegistrationRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> save(@ModelAttribute Registration registration) {

        Map<String, String> responseBody = new HashMap<>();
        Registration reg = repo.save(registration);
       
        if (null != reg) {
           
            responseBody.put(reg.toString() +" : ","Registration data added Successfully");
            return new ResponseEntity<Object>(responseBody, HttpStatus.OK);
        } else {
            responseBody.put("Failed to Save Data", "Failed");
            return new ResponseEntity<Object>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
