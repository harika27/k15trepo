package com.k15t.pat.registration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Object> handle(ConstraintViolationException exception) {

        String errorMessage = new ArrayList<>(exception.getConstraintViolations()).get(0).getMessage();
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put(errorMessage, "Please Resolve the Errors and Proceed");
        return new ResponseEntity<Object>(responseBody, HttpStatus.BAD_REQUEST);

    }
}
