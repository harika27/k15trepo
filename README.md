# Full-Stack Developer Sample Project

Note:
* The Project is developed to run on a Windows Machine.

#Features:
* The Registration form is developed to fill the details and store in H2 in memory database.
* the Fields of the form are validated and the validation messages are displayed accordingly.
* The Data is saved into the H2 database only if all the values are passed correctly.

#Technologies Used:
 * Springboot
 * H2 in memory database
 * Velocity
 * Mockito
 * JPA

#Improvements made:
* The address field is splitted into Country,State,City,Zipcode fields for better experience to the user.

#Future Scope:
* Currently, only the success or failure response is displayed to the user without formatting the output which can be improvised further.

#Database connection:
* The system uses H2 database which can be connected using
* http://localhost:8080/h2-console


